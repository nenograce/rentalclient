package rental.rent.spring.boot.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Range;
import org.springframework.context.annotation.Bean;

import javax.persistence.*;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;


@Entity(name = "user")
@Table(name = "user")
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    @Id
    public String user_id;
    public String user_name;
    public String user_mail;
    public String gender;
    @Min(2)
    public Integer experience;
    private LocalDateTime lastVisited;

    public User(){

    }
    public User(String id, String name){
        this.user_id = id;
        this.user_name = name;
    }
    public String getUserId() {
        return user_id;
    }
    public void setUserId(String userId) {
        this.user_id = userId;
    }
    public String getUserName() {
        return user_name;
    }
    public void setUserName(String user_name) {
        this.user_name = user_name;
    }
    public String getUserMail() {
        return user_mail;
    }
    public void setUserMail(String user_mail) {
        this.user_mail = user_mail;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public Integer getExperience() {
        return experience;
    }
    public void setExperience(Integer experience) {
        this.experience = experience;
    }
    public LocalDateTime getLastVisited() {
        return lastVisited;
    }
    public void setLastVisited(LocalDateTime time) {
        this.lastVisited = time;
    }
    }

