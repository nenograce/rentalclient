package rental.rent.spring.boot.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import rental.rent.spring.boot.model.User;
@Component
@Service
public interface UserService {
    List<User> listAllUsers();
    public List<User> getAllUserArticles();
    public void addUser(User user);

}