package rental.rent.spring.boot.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import rental.rent.spring.boot.model.User;
import rental.rent.spring.boot.repository.UserRepository;
import rental.rent.spring.boot.service.UserService;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

@RestController

@RequestMapping("app")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserRepository userRepository;
    List<User> usersList = new ArrayList<User>();
    UserController(UserRepository userRep){
        this.userRepository = userRep;
    }
    @RequestMapping(value = "/create-user", method = RequestMethod.GET)
    public ModelAndView createUserView() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("user-creation");
        return mav;
    }
    @RequestMapping(value = "/adding-user", method = RequestMethod.POST)
    public @ResponseBody ModelAndView createUser(@ModelAttribute User user, Model model, BindingResult result) {
        ModelAndView mav = new ModelAndView();
        if(result.hasErrors()) {
            mav.setViewName("user-creation");
            mav.addObject("user", user);
            return mav;
        }
        mav.addObject("user", userRepository.findAll());
        mav.setViewName("user-info");
        return mav;

    }
    @RequestMapping(path = "/user-info", method = RequestMethod.GET)
    public ModelAndView findAllUser(Model model) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("user-info");
        return mav;
    }


    @GetMapping("user-search/{id}")
    public ResponseEntity<User> findById(@PathVariable String id) {

        return userRepository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/user-update/{id}")
    public @ResponseBody ModelAndView edit(@PathVariable("id") String id, Model model){
        ModelAndView mav = new ModelAndView();
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid UserID:" + id));
        mav.addObject("user", user);
        mav.setViewName("user-update");
        return mav;
    }

    @PostMapping("/user-updating/{id}")
    @Transactional
    public ModelAndView updateUser(@PathVariable("id") String id, @Valid User user,
                             BindingResult result, Model model) {
        ModelAndView mav = new ModelAndView();
        if (result.hasErrors()) {
            user.setUserId(id);
            return new ModelAndView("redirect:/app/user-update/"+id);
        }
        User chanUser = userRepository.getOne(id);
        user.setUserId(chanUser.getUserId());
        user.setUserName(chanUser.getUserName());
        user.setUserMail(chanUser.getUserMail());
        userRepository.save(user);
        return new ModelAndView("redirect:/app/user-info");
    }

    @GetMapping("/user-delete/{id}")
    public ModelAndView deleteUser(@PathVariable("id") String id, Model model) {
        ModelAndView mav = new ModelAndView();
        userRepository.deleteById(id);
        mav.addObject("user", userRepository.findAll());
        mav.setViewName("user-info");
        return mav;
    }
}
