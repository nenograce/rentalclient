package rental.rent.spring.boot.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rental.rent.spring.boot.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

}
