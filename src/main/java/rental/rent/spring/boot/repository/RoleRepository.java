package rental.rent.spring.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rental.rent.spring.boot.model.User;

@Repository
public interface RoleRepository extends JpaRepository<User, String> {
}
